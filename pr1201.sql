CREATE DATABASE IF NOT EXISTS dbron13;
USE dbron13;

DROP TABLE if EXISTS symptoms_table;
DROP TABLE if EXISTS contact;
DROP TABLE if EXISTS physical_management;
DROP TABLE if EXISTS action_table;
DROP TABLE if EXISTS personal_information_table;

CREATE TABLE personal_information_table( /* 個人管理テーブル */
ID INT NOT NULL AUTO_INCREMENT, /* 主キー */
course_of_study VARCHAR(10), /* 学科 */
post_ID VARCHAR(20), /* 学籍（個人）番号 */
namae  VARCHAR(40) NOT NULL, /*氏名 */
post VARCHAR(30) NOT NULL, /*役職 */
phonenum  VARCHAR(20) NOT NULL, /*電話番号 */
cancel BOOLEAN DEFAULT FALSE, /*キャンセルフラグ */
lastupdate DATETIME, /*最終更新日 */
passward VARCHAR(30) NOT NULL, /*ログイン用パスワード */
PRIMARY KEY(ID)
);
DESC personal_information_table;

CREATE TABLE action_table( /*行動管理テーブル */
 actionID INT NOT NULL AUTO_INCREMENT, /*主キー */
 ID INT NOT NULL, /*外部キー（個人管理テーブルと結合）*/
 dates VARCHAR(20) NOT NULL, /*行動日*/
 starting_time VARCHAR(10), /*出発時刻*/
 arrival_time VARCHAR(10), /*到着時刻*/
 place_of_departure VARCHAR(30), /*出発地*/
 destination VARCHAR(30), /*目的地*/
 method_of_travel VARCHAR(30), /*移動手段*/
 fellow_travellers BOOLEAN DEFAULT FALSE, /*同行者の有無*/
 the_number_of_people INT, /*同行者の人数*/
 relation VARCHAR(30), /*同行者の関係*/
 cancel BOOLEAN DEFAULT FALSE, /*キャンセルフラグ*/
 lastupdate DATETIME, /*最終更新日*/
PRIMARY KEY(actionID),
FOREIGN KEY(ID)
      REFERENCES personal_information_table(ID)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);
DESC action_table;

CREATE TABLE physical_management( /*体調管理テーブル*/
physical_managementID INT NOT NULL AUTO_INCREMENT, /*主キー*/
ID INT NOT NULL, /*外部キー（個人管理テーブルと結合）*/
symptomsID INT NOT NULL, /*外部キー（症状テーブルと結合）*/
dates VARCHAR(20) NOT NULL, /*症状が出ている日*/
temperature DOUBLE NOT NULL, /*体温*/
entry_time datetime, /*入力日*/
corona_now BOOLEAN DEFAULT FALSE,/*今コロナかどうか*/
cancel BOOLEAN DEFAULT FALSE,/*キャンセルフラグ*/
lastupdate DATETIME,/*最終更新日*/
PRIMARY KEY(physical_managementID),
FOREIGN KEY(ID)
      REFERENCES personal_information_table(ID)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);
DESC physical_management;

CREATE TABLE contact(/*連絡*/
 contactID INT NOT NULL AUTO_INCREMENT,/*主キー*/
 ID INT NOT NULL,/*外部キー（個人管理テーブルと結合）*/
 date_of_infection VARCHAR(20) NOT NULL,/*感染日*/
 cancel BOOLEAN DEFAULT FALSE,/*キャンセルフラグ*/
 lastupdate DATETIME,/*最終更新日*/
PRIMARY KEY(contactID),
FOREIGN KEY(ID)
      REFERENCES personal_information_table(ID)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);
DESC contact;

CREATE TABLE symptoms_table( /*症状テーブル*/
symptomsID INT NOT NULL AUTO_INCREMENT UNIQUE, /*主キー*/
joint_pain_muscle_pain BOOLEAN DEFAULT FALSE, /*筋肉痛*/
washed_out_feeling BOOLEAN DEFAULT FALSE, /*だるさ*/
headache BOOLEAN DEFAULT FALSE, /*頭痛*/
sore_throat BOOLEAN DEFAULT FALSE,/*咽頭痛*/
breathless BOOLEAN DEFAULT FALSE, /*呼吸困難*/
cough_sneezing BOOLEAN DEFAULT FALSE,/*咳・くしゃみ*/
nausea_vomiting BOOLEAN DEFAULT FALSE,/*吐気・嘔吐*/
abdominal_pain_diarrhea BOOLEAN DEFAULT FALSE,/*腹痛・下痢*/
taste_disorder BOOLEAN DEFAULT FALSE,/*味覚障害*/
olfactory_disorder BOOLEAN DEFAULT FALSE,/*嗅覚障害*/
cancel BOOLEAN DEFAULT FALSE,
lastupdate datetime,
PRIMARY KEY(symptomsID)
);
DESC symptoms_table;