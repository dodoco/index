from Mydatabase import my_open , my_query,my_close
from datetime import timedelta
import pandas as pd
from crypt import methods
from email import message
import os
from datetime import datetime,timedelta
import pandas as pd
from flask import Flask, redirect, url_for, render_template, request, session, redirect, url_for, request
import requests
import streamlit as st
from markupsafe import escape

dsn = {
    'host' : '172.30.0.10',  #ホスト名(IPアドレス)
    'port' : '3306',        #mysqlの接続ポート番号
    'user' : 'root',      #dbアクセスするためのユーザid
    'password' : '1234',    #ユーザidに対応するパスワード
    'database' : 'dbron13' #オープンするデータベース名
}

app = Flask(__name__)

app.secret_key = 'abcdefghijklmn'
app.permanent_session_lifetime = timedelta(minutes=3)

@app.route("/", methods = ["POST", "GET"])
def login():
  if request.method == "POST":
    session.permanent = True
    user = request.form.get("post_ID")
    key = request.form.get("passward")
    session["post_ID"] = user
    session["passward"] = key
    dbcon,cur = my_open(**dsn)
    sqlstring=f"""
       SELECT post_ID, passward,ID
       FROM personal_information_table
       WHERE post_ID ='{user}'
        ;
    """
    my_query(sqlstring,cur)
    df = pd.DataFrame(cur.fetchall())
    b = df.iloc[0]['post_ID']
    print(b)
    a = df.iloc[0]['passward']
    session["ID"]=str(df.iloc[0]['ID'])
    print(a)
    print(key)
    if a == key and b==user:
      return redirect(url_for("login_"))
    elif b==user and a!=key :
      session.pop('post_ID',None)
      session.pop('passward',None)
      session.pop('ID',None)
      session.clear()
      return render_template("false.html",
          message = "パスワードが違います"
          )
    elif len(b)==0:
      return render_template("false.html",
          message = "学籍番号が違います"
          )
    else:
       session.pop('post_ID',None)
       session.pop('passward',None)
       session.pop('ID',None)
       session.clear()
       return render_template("false.html",
          message = "ログインしっぱい"
          )
  else:
    if "post_ID" in session:
      return redirect(url_for("login_"))
    return render_template("index.html")

@app.route("/login", methods=["GET","POST"])
def login_():
  if "post_ID" in session:
    return render_template("login.html", post_ID=session["post_ID"],key=session["passward"])
  return render_template("index.html")

@app.route("/logout", methods=["GET"])
def logout():
  session.pop('post_ID',None)
  session.pop('passward',None)
  session.pop('ID',None)
  session.clear()
  return redirect("/")

@app.route("/taityou_form", methods=["POST","GET"])
def taityou():
    return render_template( "taityou_form.html",
    title = "体調観察入力画面",
    post_ID=session["post_ID"]
    )

@app.route("/taityou2", methods=["POST"])
def taityou2():
    namae =request.form["namae"]
    taion = request.form["taion"]
    date = request.form["date"]
    ID = int(session["ID"])
    syouzyou1 = request.form["syouzyou1"]
    syouzyou2 = request.form["syouzyou2"]
    syouzyou3 = request.form["syouzyou3"]
    syouzyou4 = request.form["syouzyou4"]
    syouzyou5 = request.form["syouzyou5"]
    syouzyou6 = request.form["syouzyou6"]
    syouzyou7 = request.form["syouzyou7"]
    syouzyou8 = request.form["syouzyou8"]
    syouzyou9 = request.form["syouzyou9"]
    syouzyou10 = request.form["syouzyou10"]
    dt_now = datetime.now()
    dbcon,cur = my_open( **dsn )
    #SQLの設定と，クエリ実行
    sqlstring = f"""

        INSERT INTO physical_management
        (ID,dates,temperature,lastupdate,joint_pain_muscle_pain,washed_out_feeling,headache,sore_throat,breathless,cough_sneezing,nausea_vomiting,abdominal_pain_diarrhea,taste_disorder,olfactory_disorder)
        VALUES
        ({ID},'{date}',{taion},'{dt_now}','{syouzyou1}','{syouzyou2}','{syouzyou3}','{syouzyou4}','{syouzyou5}','{syouzyou6}','{syouzyou7}','{syouzyou8}','{syouzyou9}','{syouzyou10}')
        ;
    """
    my_query(sqlstring,cur)
    #DB書き込み
    dbcon.commit()
    cnt=0
    syouzyou = [syouzyou1,syouzyou2,syouzyou3,syouzyou4,syouzyou5,syouzyou6,syouzyou7,syouzyou8,syouzyou9,syouzyou10]
    for i in syouzyou :
        if int(i) == 1:
            cnt += 1
    if cnt >= 5 :
        dbcon,cur = my_open( **dsn )
        #SQLの設定と，クエリ実行
        sqlstring = f"""

            INSERT INTO pick_up
            (ID,date_of_infection,lastupdate)
            VALUES
            ('{ID}','{date}','{dt_now}')
            ;
        """
        my_query(sqlstring,cur)
        #DB書き込み
        dbcon.commit()
    elif float(taion) > 37.5:
        dbcon,cur = my_open( **dsn )
        #SQLの設定と，クエリ実行
        sqlstring = f"""

            INSERT INTO pick_up
            (ID,date_of_infection,lastupdate)
            VALUES
            ('{ID}','{date}','{dt_now}')
            ;
        """
        my_query(sqlstring,cur)
        #DB書き込み
        dbcon.commit()
    return render_template( "msg.html",
    message =f"{namae}さんの行動が記録されました。"
    )

@app.route("/koudou_form", methods=["POST","GET"])
def koudou():
    return render_template( "koudou_form.html" ,
    title = "行動入力画面",
    post_ID=session["post_ID"]
    )

@app.route("/koudou_form2", methods=["POST"])
def koudou2():

    ID = int(session["ID"])
    namae = request.form["namae"]
    dates = request.form["dates"]
    starting_time = request.form["starting_time"]
    arrival_time = request.form["arrival_time"]
    place_of_departure = request.form["place_of_departure"]
    destination = request.form["destination"]
    method_of_travel = request.form["method_of_travel"]
    fellow_travellers = request.form["fellow_travellers"]
    the_number_of_people = request.form["doukou"]
    relation = request.form["relation"]
    dt_now = datetime.now()


    dbcon,cur = my_open( **dsn )
    #SQLの設定と，クエリ実行
    sqlstring = f"""
        INSERT INTO action_table
        (ID,dates,starting_time,arrival_time,place_of_departure,destination,method_of_travel,fellow_travellers,the_number_of_people,relation,lastupdate)
        VALUES
        ({ID},'{dates}','{starting_time}','{arrival_time}','{place_of_departure}','{destination}','{method_of_travel}',{fellow_travellers},{the_number_of_people},'{relation}','{dt_now}')
        ;
    """
    my_query(sqlstring,cur)
    #DB書き込み
    dbcon.commit()

    return render_template( "msg.html" ,
    title = "行動入力画面",
    message =f"{namae}さんの行動が記録されました。"
    )

#感染・濃厚接触フォーム
@app.route("/infection_form", methods=["POST","GET"])
def infection_form():
    return render_template("infection_form.html",
        title = "感染・濃厚接触テーブル",
        post_ID = session["post_ID"]
    )

@app.route("/infection_form1",methods=["POST"])
def infection_form1():

    namae = request.form["namae"]
    ID = int(session["ID"])
    date = request.form["date"]
    dt_now = datetime.now()
    #dbオープン
    dbcon,cur = my_open( **dsn )
    # inf_date = datetime.strptime(request.form["inf_date"],'%Y-%m-%d')
    # end_date = inf_date + timedelta(days=7)
    # print(end_date)
    #SQLの設定と，クエリ実行
    sqlstring = f"""

        INSERT INTO contact
        (ID,date_of_infection,lastupdate)
        VALUES
        ({ID},'{date}','{dt_now}')
        ;
    """
    my_query(sqlstring,cur)
    #DB書き込み
    dbcon.commit()
    message = f"{namae}さんの記録を追加しました"

    return render_template( "msg.html",
        title="レコード新規追加",
        message = message
    )


if __name__ == '__main__':
    app.debug = True
    app.run(host='localhost')

#プログラム起動
app.run(host="localhost",port=5000,debug=True)
