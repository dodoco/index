from Mydatabase import my_open , my_query,my_close
from datetime import timedelta
import pandas as pd
from crypt import methods
from email import message
import os
from datetime import datetime,timedelta
import pandas as pd
from flask import Flask, redirect, url_for, render_template, request, session, redirect, url_for, request
import requests
import streamlit as st
from markupsafe import escape

dsn = {
    'host' : '172.30.0.10',  #ホスト名(IPアドレス)
    'port' : '3306',        #mysqlの接続ポート番号
    'user' : 'root',      #dbアクセスするためのユーザid
    'password' : '1234',    #ユーザidに対応するパスワード
    'database' : 'dbron13' #オープンするデータベース名
}

app = Flask(__name__)

app.secret_key = 'abcdefghijklmn'
app.permanent_session_lifetime = timedelta(minutes=3)

@app.route("/", methods = ["POST", "GET"])
def login():
  if request.method == "POST":
    session.permanent = True
    user = request.form.get("post_ID")
    key = request.form.get("passward")
    session["post_ID"] = user
    session["passward"] = key
    dbcon,cur = my_open(**dsn)
    sqlstring=f"""
       SELECT post_ID, passward,ID
       FROM personal_information_table
       WHERE post_ID ='{user}'
        ;
    """
    my_query(sqlstring,cur)
    df = pd.DataFrame(cur.fetchall())
    b = df.iloc[0]['post_ID']
    print(b)
    a = df.iloc[0]['passward']
    session["ID"]=str(df.iloc[0]['ID'])
    print(a)
    print(key)
    if a == key and b==user:
      return redirect(url_for("login_"))
    elif b==user and a!=key :
      session.pop('post_ID',None)
      session.pop('passward',None)
      session.pop('ID',None)
      session.clear()
      return render_template("false.html",
          message = "パスワードが違います"
          )
    elif len(b)==0:
      return render_template("false.html",
          message = "学籍番号が違います"
          )
    else:
       session.pop('post_ID',None)
       session.pop('passward',None)
       session.pop('ID',None)
       session.clear()
       return render_template("false.html",
          message = "ログインしっぱい"
          )
  else:
    if "post_ID" in session:
      return redirect(url_for("login_"))
    return render_template("index.html")

@app.route("/login", methods=["GET","POST"])
def login_():
  if "post_ID" in session:
    return render_template("login-2.html", post_ID=session["post_ID"],key=session["passward"])
  return render_template("index.html")

@app.route("/logout", methods=["GET"])
def logout():
  session.pop('post_ID',None)
  session.pop('passward',None)
  session.pop('ID',None)
  session.clear()
  return redirect("/")

@app.route("/select_infection")
def select():

    dbcon,cur = my_open( **dsn )
    #レコード新規挿入のSQL文
    sqlstring = f"""
        SELECT course_of_study,personal_information_table.post_ID,namae,phonenum,date_of_infection,contact.lastupdate
        FROM contact
        LEFT JOIN personal_information_table
        ON contact.ID = personal_information_table.ID
        WHERE contact.cancel=0
    """
    my_query(sqlstring,cur)
    recset = pd.DataFrame( cur.fetchall() )

    return render_template( "table.html",
        title = "感染者、濃厚接触者及び感染の疑い一覧",
        table_data = recset,
        post_ID = session["post_ID"]
    )

@app.route("/update_infection")
def update_infection():
    dbcon,cur = my_open( **dsn )
    #レコード新規挿入のSQL文
    sqlstring = f"""
        SELECT contactID,date_of_infection,cancel,lastupdate
        FROM contact
        ;
    """
    my_query(sqlstring,cur)
    recset = pd.DataFrame( cur.fetchall() )

    my_close( dbcon,cur )

    return render_template( "update_select.html",
        title = "contactテーブルのレコード",
        table_data = recset,
        post_ID = session["post_ID"]
    )

@app.route("/update_infection2",methods=["POST"])
def update2():
    dbcon,cur = my_open( **dsn )
    contactID = request.form["contactID"]
    #レコード新規挿入のSQL文
    sqlstring = f"""
        SELECT *
        FROM contact
        WHERE contactID = {contactID}
        ;
    """
    my_query(sqlstring,cur)
    recset = pd.DataFrame( cur.fetchall() )
    #DataFrame形式(2次元)をSeries形式(1次元ベクトルデータ)に変換する
    rowdata = pd.Series( recset.iloc[0] )

    my_close( dbcon,cur )

    return render_template("update_form.html",
        title=f"{contactID}件目の更新",
        table_data = rowdata
    )

@app.route("/update_infection3",methods=["POST"])
def update_infection3():
    dbcon,cur = my_open( **dsn )
    contactID = request.form["contactID"]
    namae = request.form["namae"]
    date = request.form["date"]
    cancel = request.form["cancel"]
    dt_now = datetime.now()
    #レコード更新のsql
    sqlstring = f"""
        UPDATE contact
        SET date_of_infection = '{date}',
        cancel = {cancel},
        lastupdate = '{dt_now}'
        WHERE contactID = {contactID};
    """
    my_query(sqlstring,cur)
    dbcon.commit()
    my_close( dbcon,cur )

    return render_template( "msg.html",
        title = "レコード更新完了",
        message = f"{namae}さんの{contactID}件目のレコードを更新しました"

    )

@app.route("/delete_infection")
def delete_infection():
    dbcon,cur = my_open( **dsn )

    #レコード新規挿入のSQL文
    sqlstring = f"""
        SELECT contactID,date_of_infection,cancel,lastupdate
        FROM contact
        WHERE cancel='TRUE'
        ;
    """
    my_query(sqlstring,cur)
    recset = pd.DataFrame( cur.fetchall() )

    my_close( dbcon,cur )

    return render_template( "delete_select.html",
        title = "contactテーブルのレコード",
        table_data = recset,
        post_ID = session["post_ID"]
    )

@app.route("/delete_infection2",methods=["POST"])
def delete_infection2():
    dbcon,cur = my_open( **dsn )
    contactID = request.form["contactID"]
    dt_now = datetime.now()

    #レコード更新のsql
    sqlstring = f"""
        UPDATE contact
        SET cancel = TRUE,
        lastupdate = '{dt_now}'
        WHERE contactID = {contactID}
    """
    my_query(sqlstring,cur)
    dbcon.commit()
    my_close( dbcon,cur )

    return render_template( "msg.html",
        title = "レコード削除完了",
        message = f"{contactID}のレコードを削除しました"

    )

@app.route("/select_pick_up")
def select_pick_up():

    dbcon,cur = my_open( **dsn )
    #レコード新規挿入のSQL文
    sqlstring = f"""
        SELECT course_of_study,personal_information_table.post_ID,namae,phonenum,date_of_infection,pick_up.lastupdate
        FROM pick_up
        LEFT JOIN personal_information_table
        ON pick_up.ID = personal_information_table.ID
        WHERE pick_up.cancel=0
    """
    my_query(sqlstring,cur)
    recset = pd.DataFrame( cur.fetchall() )

    return render_template( "table.html",
        title = "感染の疑い一覧",
        table_data = recset,
        post_ID = session["post_ID"]
    )

@app.route("/delete_pick_up")
def delete_pick_up():
    dbcon,cur = my_open( **dsn )

    #レコード新規挿入のSQL文
    sqlstring = f"""
        SELECT pick_upID,date_of_infection,cancel,lastupdate
        FROM pick_up
        WHERE cancel=0
        ;
    """
    my_query(sqlstring,cur)
    recset = pd.DataFrame( cur.fetchall() )

    my_close( dbcon,cur )

    return render_template( "delete_pick_up.html",
        title = "contactテーブルのレコード",
        table_data = recset,
        post_ID = session["post_ID"]
    )

@app.route("/delete_pick_up2",methods=["POST"])
def delete_pick_up2():
    dbcon,cur = my_open( **dsn )
    pick_upID = request.form["pick_upID"]
    dt_now = datetime.now()

    #レコード更新のsql
    sqlstring = f"""
        UPDATE pick_up
        SET cancel = TRUE,
        lastupdate = '{dt_now}'
        WHERE pick_upID = {pick_upID}
    """
    my_query(sqlstring,cur)
    dbcon.commit()
    my_close( dbcon,cur )

    return render_template( "msg.html",
        title = "レコード削除完了",
        message = f"{pick_upID}のレコードを削除しました"

    )

@app.route("/select_action")
def select_action():
    return render_template( "search.html",
        title = "行動管理一覧",
        post_ID = session["post_ID"]
    )

@app.route("/select_action2",methods=["POST"])
def select_action2():
    num = request.form["num"]
    dbcon,cur = my_open( **dsn )
    #レコード新規挿入のSQL文
    sqlstring = f"""
        SELECT post_ID,namae,post,phonenum,dates,starting_time,arrival_time,place_of_departure,destination,method_of_travel,the_number_of_people,relation,action_table.lastupdate
        FROM action_table
        LEFT JOIN personal_information_table
        ON action_table.ID = personal_information_table.ID
        WHERE post_ID = '{num}'
    """
    my_query(sqlstring,cur)
    recset = pd.DataFrame( cur.fetchall() )

    return render_template( "table.html",
        title = f"{num}の行動管理一覧",
        table_data = recset,
        post_ID = session["post_ID"]
    )

@app.route("/new_acount")
def new_acount():
    return render_template( "new_acount.html",
        title = "新規登録",
        post_ID = session["post_ID"]
    )

@app.route("/new_acount2",methods=["POST"])
def new_acount2():
    course_of_study = request.form["course_of_study"]
    post_ID2= request.form["post_ID2"]
    namae= request.form["namae"]
    post= request.form["post"]
    phonenum= request.form["phonenum"]
    passward= request.form["passward"]
    dt_now = datetime.now()
    dbcon,cur = my_open( **dsn )
    sqlstring = f"""
        INSERT INTO personal_information_table
        (course_of_study,post_ID,namae,post,phonenum,passward,lastupdate)
        VALUES
        ('{course_of_study}','{post_ID2}','{namae}','{post}','{phonenum}','{passward}','{dt_now}')
        ;
    """
    my_query(sqlstring,cur)
    dbcon.commit()
    my_close( dbcon,cur )
    return render_template( "msg.html",
        title = "新規登録",
        post_ID = session["post_ID"],
        message = "登録完了"
    )

if __name__ == '__main__':
    app.debug = True
    app.run(host='localhost')

#プログラム起動
app.run(host="localhost",port=5000,debug=True)
